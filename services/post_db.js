const { Pool } = require("pg")
const pool = new Pool({
    host: "localhost",
    user: "postgres",
    password: "postgres",
    database: "website",
    port: 5432,
})

async function createPost(post) {
    let id = 0
    let client = await pool.connect()
    try {
        let sql = "INSERT INTO posts (title, body) VALUES ($1, $2) RETURNING id"
        let res = await client.query(sql, [post.title, post.body])
        id = res.rows[0].id
    } catch (e) {
        console.log(`error: ${e.message}`)
    } finally {
        client.release()
    }

    return id
}

async function readPost(id) {
    let result = {}
    let client = await pool.connect()
    try {
        let sql = "SELECT * FROM posts WHERE id = $1"
        let res = await client.query(sql, [id])
        result = res.rows[0]
    } catch (e) {
        console.log(`error: ${e.message}`)
    } finally {
        client.release()
    }

    return result
}

async function readAllPost() {
    let result = []
    let client = await pool.connect()
    try {
        let sql = "SELECT * FROM posts"
        let res = await client.query(sql)
        result = res.rows
    } catch (e) {
        console.log(`error: ${e.message}`)
    } finally {
        client.release()
    }

    return result
}

async function updatePost(id, post) {
    let client = await pool.connect()
    try {
        let sql = "UPDATE posts SET title = $1, body = $2 WHERE id = $3"
        await client.query(sql, [post.title, post.body, id])
    } catch (e) {
        console.log(`error: ${e.message}`)
    } finally {
        client.release()
    }
}

async function deletePost(id) {
    let client = await pool.connect()
    try {
        let sql = "DELETE FROM posts WHERE id = $1"
        await client.query(sql, [id])
    } catch (e) {
        console.log(`error: ${e.message}`)
    } finally {
        client.release()
    }
}

module.exports = {
    createPost,
    readPost,
    getDatabase: readAllPost,
    updatePost,
    deletePost
}
// deletePost(2)
// updatePost(1, {title: "Hello world", body: "Hello world"})
// readAllPost().then(data => console.log(data))
// readPost(3).then(data => console.log(data))
// createPost({title: "Sample 2", body: "Hello world 2"}).then(id => console.log(id))