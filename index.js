const express = require("express")
const app = express()

const post = require("./api/post")
/**
 * index.js
 *     |
 *     V
 * api/post.js
 *     |
 *     V
 * services/post_data.js
 *     |
 *     V
 * database/post.json
 */
app.use("/api/v1/posts", post)

app.listen(3000, () => {
    console.log(`server running on port 3000`)
})